#!/bin/sh

# Ensure jq is installed
if ! command -v jq >/dev/null 2>&1; then
  echo "jq not found. Installing jq..."
  apt-get update && apt-get install -y jq
  echo "jq installed."
fi

# Get the repository name from GitLab CI environment variables
REPO_NAME=$CI_PROJECT_NAME

echo "Repository name: $REPO_NAME"
echo "CI_COMMIT_SHA: $CI_COMMIT_SHA"
echo "CI_COMMIT_REF_NAME: $CI_COMMIT_REF_NAME"
echo "CI_MERGE_REQUEST_IID: $CI_MERGE_REQUEST_IID"
echo "CI_PROJECT_ID: $CI_PROJECT_ID"
echo "MR_TOKEN: $MR_TOKEN"

#### Analyze code
echo "Running sl analyze..."
sl analyze --app QwietAI-Kot --wait --verbose --version-id "$CI_COMMIT_SHA" --tag branch="$CI_COMMIT_REF_NAME" .
echo "sl analyze completed."

#### Run build rules

# Run check-analysis and save report to /tmp/check-analysis.md
echo "Running sl check-analysis..."
sl check-analysis --app QwietAI-Kot --report --report-file /tmp/check-analysis.md --source "tag.branch=master" --target "tag.branch=$CI_COMMIT_REF_NAME"
echo "sl check-analysis completed."

CHECK_ANALYSIS_OUTPUT=$(cat /tmp/check-analysis.md)
echo "Check analysis output:"
echo "$CHECK_ANALYSIS_OUTPUT"

COMMENT_BODY=$(jq -n --arg body "$CHECK_ANALYSIS_OUTPUT" '{body: $body}')

# Check if this is running in a merge request
if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  echo "Got merge request $CI_MERGE_REQUEST_IID for branch $CI_COMMIT_REF_NAME"
  
  # Post report as merge request comment
  echo "Posting report as merge request comment..."
  curl -i -X POST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
      -H "PRIVATE-TOKEN: $MR_TOKEN" \
      -H "Content-Type: application/json" \
      -d "$COMMENT_BODY"
  echo "Report posted."
fi

